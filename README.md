WolfyBot
========
#### TravisCI Automated Build
[![Build Status](https://travis-ci.org/wolfy1339/WolfyBot.svg)](https://travis-ci.org/wolfy1339/WolfyBot)
***
Lua IRC bot.

IRC library from https://github.com/JakobOvrum/LuaIRC ,
Based off of [Crackbot](https://github.com/cracker64/Crackbot)
